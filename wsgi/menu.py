#coding: utf-8



# 第一層標題
m1 = "<a href='/about'>有關本站</a>"
m11 = "<a href='/'>Brython程式區</a>"
m111 = "<a href=''>四連桿機構</a>"
m112 = "<a href=''>零件體積表列</a>"
m12 = "<a href='/creoParts'>Creo 零件</a>"
# 第一層標題
m2 = "<a href=''>期末報告</a>"
m21 = "<a href='/pdf'>期末報告pdf檔案下載區</a>"
m22 = "<a href='/stl'>STL零件</a>"
# 第一層標題
m3 = "<a href=''>參考資料</a>"
m31 = "<a href=''>Creo</a>"
m311 = "<a href='/creo1'>零件繪圖</a>"
m312 = "<a href='/creo2'>零件組立</a>"
m33 = "<a href=''>Solvespace</a>"
m331 ="<a href='/solvespace1'>零件繪圖</a>"
m332 ="<a href='/solvespace2'>零件組立</a>"
m34 = "<a href='/openshiftandbitbucket'>openshift與bitbucket同步</a>"
# 第一層標題
m4 = "<a href=''>組員介紹</a>"
m41 = "<a href='/introMember1'>吳羽閔_16</a>"
m42 = "<a href='/introMember2'>吳謦麟_18</a>"
m43 = "<a href='/introMember3'>林子航_22</a>"
m44 = "<a href='/introMember4'>林育民_23</a>"
m45 = "<a href='/introMember5'>戴志軒_53</a>"

# 請注意, 數列第一元素為主表單, 隨後則為其子表單
表單數列 = [ \
        [m1, [m11, m111, m112], m12],  \
        [m2, m21, m22], \
        [m3, [m31, m311, m312], [m33,m331,m332], m34], \
        [m4, m41, m42, m43, m44,m45]
        ]

def SequenceToUnorderedList(seq, level=0):
    # 只讓最外圍 ul 標註 class = 'nav'
    if level == 0:
        html_code = "<ul class='nav'>"
    # 子表單不加上 class
    else:
        html_code = "<ul>"
    for item in seq:
        if type(item) == type([]):
            level += 1
            html_code +="<li>"+ item[0]
            # 子表單 level 不為 0
            html_code += SequenceToUnorderedList(item[1:], 1)
            html_code += "</li>"
        else:
            html_code += "<li>%s</li>" % item
    html_code += "</ul>\n"
    return html_code

def GenerateMenu():
    return SequenceToUnorderedList(表單數列, 0) \
+'''
<script type="text/javascript" src="/static/jscript/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="/static/jscript/dropdown.js"></script>
'''
